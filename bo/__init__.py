from flask import Flask
from .helpers import init_user
from . import db
from . import entrypoints
from . import api
import config


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    env_class = getattr(config, f'{app.config["ENV"].capitalize()}Conf')
    app.config.from_object(env_class)
    db.init_app(app)
    app.before_request(init_user)
    app.register_blueprint(entrypoints.bp)
    app.register_blueprint(api.login.bp)
    app.register_blueprint(api.account.bp)
    app.register_blueprint(api.code.bp)
    app.register_blueprint(api.agreement.bp)
    app.register_blueprint(api.executor.bp)
    app.register_blueprint(api.payment.bp)
    app.register_blueprint(api.annual_plan.bp)
    env_class().post_init(app)
    return app
