from flask import Blueprint, jsonify, g
from flask_request_validator import GET, JSON, Param, validate_params
from peewee import JOIN, fn
from functools import reduce
from datetime import datetime
from decimal import Decimal
from .. import strings
from ..helpers import authorized_api, permissions, mysql_date
from ..models import Account, AccountCode, Code, AccountLog, User, Payment
import operator
import math

bp = Blueprint('api_account', __name__)


def _get_account_dict(acc, codes):
    result = {
        'id': acc.id,
        'number': acc.number,
        'name': acc.name,
        'type': acc.type,
        'sum': 0,
        'codes': {}
    }
    if codes:
        for acc_code in codes:
            result['codes'][acc_code.code.code] = {
                'id': acc_code.id,
                'name': acc_code.code.description,
                'balance': acc_code.balance
            }
            result['sum'] += acc_code.balance
    return result


def _get_odeku_okkt_sum(year):
    sum_codes = (AccountCode
                 .select(fn.SUM(AccountCode.balance).alias('balance'), Code, Account.number)
                 .join_from(AccountCode, Code)
                 .join_from(AccountCode, Account)
                 .where((Account.type << (Account.TYPE_ODEKU, Account.TYPE_OKKT)) & (Account.year == year))
                 .group_by(AccountCode.code, Account.number))
    sums = {}
    for sum_code in sum_codes:
        balance = Decimal(sum_code.balance)
        sums.setdefault(sum_code.account.number, {'sum': Decimal(), 'codes': {}})['codes'][sum_code.code.code] = {
            'balance': balance,
            'name': sum_code.code.description
        }
        sums[sum_code.account.number]['sum'] += balance
    result = []
    for number, sum_data in sums.items():
        result.append({
            'nolink': True,
            'number': number,
            'codes': sum_data['codes'],
            'sum': sum_data['sum']
        })
    return result


def _account_log_undoredo(log_id, undo):
    def update_balances(alog):
        amount = alog.amount if alog.type == (AccountLog.TYPE_CREDIT if undo else AccountLog.TYPE_DEBIT) \
            else -alog.amount
        alog.account_code.balance = AccountCode.balance + amount
        alog.account_code.save()
        alog.state = AccountLog.STATE_CANCELED if undo else AccountLog.STATE_ACTIVE
        alog.user = g.user
        alog.save()

    try:
        log = AccountLog.get((AccountLog.id == log_id)
                             & (AccountLog.state == AccountLog.STATE_ACTIVE if undo else AccountLog.STATE_CANCELED))
    except AccountLog.DoesNotExist:
        return jsonify(status=False, message=strings.log_not_found), 404

    if log.payment:
        return jsonify(status=False, message=strings.log_has_payment), 404

    with g.db.atomic():
        update_balances(log)
        if log.related_log:
            update_balances(log.related_log)
    return jsonify(result=True)


@bp.route('/api/accounts', methods=('GET',))
@authorized_api
@permissions(read=True)
@validate_params(Param('year', GET, int, False))
def accounts(year):
    accs = Account.select().where(Account.year == year)
    acc_ids = {acc.id for acc in accs}
    codes = AccountCode.select(AccountCode, Code).join(Code).where(AccountCode.account << acc_ids)
    codes_by_acc = {}
    for acc_code in codes:
        codes_by_acc.setdefault(acc_code.account_id, []).append(acc_code)
    data = {}
    for acc in accs:
        acc_dict = _get_account_dict(acc, codes_by_acc.get(acc.id))
        data.setdefault(Account.type_aliases[acc.type], []).append(acc_dict)

    data['odeku-okkt'] = _get_odeku_okkt_sum(year)
    response = {
        'accounts': data,
        'order': (
            Account.type_aliases[Account.TYPE_ODEKU],
            Account.type_aliases[Account.TYPE_OKKT],
            'odeku-okkt',
            Account.type_aliases[Account.TYPE_ND4]
        )
    }
    return jsonify(result=True, data=response)


@bp.route('/api/account.get', methods=('GET',))
@authorized_api
@permissions(read=True)
@validate_params(Param('id', GET, int))
def account(aid):
    try:
        acc = Account[aid]
    except Account.DoesNotExist:
        return jsonify(result=False, message=strings.account_not_found), 404
    codes = AccountCode.select(AccountCode, Code).join(Code).where(AccountCode.account == aid)
    data = _get_account_dict(acc, codes)
    data['typeText'] = getattr(strings, f'type_{Account.type_aliases[acc.type]}')
    return jsonify(result=True, data=data)


@bp.route('/api/account.logs.get', methods=('GET',))
@authorized_api
@permissions(read=True)
@validate_params(Param('id', GET, int), Param('page', GET, int, False, 1), Param('limit', GET, int, False, 20))
def account_logs(aid, page, limit):
    related_a = AccountLog.alias()
    acc_code_a = AccountCode.alias()
    acc_a = Account.alias()
    code_a = Code.alias()
    logs = (AccountLog
            .select(AccountLog, AccountCode, Code, User, related_a, acc_code_a, acc_a, code_a, Payment)
            .join_from(AccountLog, AccountCode)
            .join_from(AccountCode, Code)
            .join_from(AccountLog, User)
            .join_from(AccountLog, related_a, on=(AccountLog.related_log == related_a.id), join_type=JOIN.LEFT_OUTER)
            .join_from(related_a, acc_code_a, join_type=JOIN.LEFT_OUTER)
            .join_from(acc_code_a, code_a, join_type=JOIN.LEFT_OUTER)
            .join_from(related_a, acc_a, join_type=JOIN.LEFT_OUTER)
            .join_from(AccountLog, Payment, join_type=JOIN.LEFT_OUTER)
            .where(AccountLog.account == aid)
            .order_by(AccountLog.created.desc()))
    data = {'data': [], 'pageCount': math.ceil(logs.count() / limit)}
    for log in logs.paginate(page, limit):
        log_dict = {
            'id': log.id,
            'type': log.type,
            'state': log.state,
            'created': log.created.strftime('%d.%m.%Y %H:%M:%S'),
            'amount': log.amount,
            'user': {'login': log.user.login},
            'account_code': {'code': log.account_code.code.code, 'description': log.account_code.code.description},
            'doc_number': log.doc_number if log.doc_number else '-',
            'doc_date': log.doc_date.strftime('%d.%m.%Y') if log.doc_date else '-'
        }
        if log.related_log:
            log_dict['related_log'] = {
                'id': log.related_log.id,
                'account': log.related_log.account.name,
                'code': log.related_log.account_code.code.code,
                'description': log.related_log.account_code.code.description
            }
        if log.payment:
            log_dict['payment'] = {'number': log.payment.number, 'agreement': log.payment.agreement_id}
        data['data'].append(log_dict)
    return jsonify(result=True, data=data)


@bp.route('/api/account.addfunds', methods=('POST',))
@authorized_api
@permissions(read=True, create=True, update=True)
@validate_params(
    Param('id', JSON, int),
    Param('code', JSON, int),
    Param('amount', JSON, float),
    Param('number', JSON, str),
    Param('date', JSON, str))
def account_addfunds(aid, code_id, amount, number, date):
    try:
        acc = Account[aid]
    except Account.DoesNotExist:
        return jsonify(result=False, message=strings.account_not_found), 404
    
    try:
        code = Code[code_id]
    except Code.DoesNotExist:
        return jsonify(result=False, message=strings.code_not_found), 404

    with g.db.atomic():
        amount = Decimal(f'{amount:.2f}')
        try:
            acc_code = AccountCode.get((AccountCode.account == acc) & (AccountCode.code == code))
            acc_code.balance = AccountCode.balance + amount
            acc_code.updated = datetime.now()
            acc_code.save()
        except AccountCode.DoesNotExist:
            acc_code = AccountCode.create(account=acc, code=code, balance=amount)
        AccountLog.create(
            account=acc,
            account_code=acc_code,
            created=datetime.now(),
            type=AccountLog.TYPE_DEBIT if amount > 0 else AccountLog.TYPE_CREDIT,
            amount=abs(amount),
            user=g.user,
            doc_number=number,
            doc_date=mysql_date(date)
        )

    acc_code = AccountCode.get_by_id(acc_code.id)
    return jsonify(result=True, data={'balance': acc_code.balance})


@bp.route('/api/accounts.search', methods=('POST',))
@authorized_api
@permissions(read=True)
@validate_params(Param('type', JSON, int, False), Param('q', JSON, str))
def accounts_search(atype, query):
    where = [(Account.current == 1)]
    if atype:
        where.append((Account.type == atype))
    if query and len(query) >= 2:
        where.append(((Account.number.contains(query)) | (Account.name.contains(query))))
    accs = Account.select().where(reduce(operator.and_, where)).limit(8)
    data = []
    for acc in accs:
        data.append({'id': acc.id, 'name': f'{acc.number} {acc.name}', 'text': f'{acc.number} {acc.name}'})
    return jsonify(result=True, data=data)


@bp.route('/api/account.code.search', methods=('POST',))
@authorized_api
@permissions(read=True)
@validate_params(Param('accountId', JSON, int))
def account_code_search(account_id):
    acc_codes = (AccountCode
                 .select(AccountCode, Code)
                 .join(Code)
                 .where((AccountCode.account == account_id) & (AccountCode.balance > 0)))
    data = []
    for acc_code in acc_codes:
        data.append({
            'id': acc_code.id,
            'name': f'{acc_code.code.code} {acc_code.code.description} ({acc_code.balance})',
            'code': acc_code.code_id
        })
    return jsonify(result=True, data=data)


@bp.route('/api/account.movefunds', methods=('POST',))
@authorized_api
@permissions(read=True, create=True, update=True)
@validate_params(
    Param('fromAccountCode', JSON, int),
    Param('code', JSON, int),
    Param('amount', JSON, float),
    Param('id', JSON, int),
    Param('number', JSON, str),
    Param('date', JSON, str))
def account_movefunds(from_account_code, code_id, amount, aid, number, date):
    try:
        acc = Account[aid]
    except Account.DoesNotExist:
        return jsonify(result=False, message=strings.account_not_found), 404
    
    try:
        code = Code[code_id]
    except Code.DoesNotExist:
        return jsonify(result=False, message=strings.code_not_found), 404
    
    try:
        source_acc_code = AccountCode[from_account_code]
    except AccountCode.DoesNotExist:
        return jsonify(result=False, message=strings.account_not_found), 404
    
    if source_acc_code.balance < amount:
        return jsonify(result=False, message=strings.not_enough_balance), 400

    with g.db.atomic():
        amount = Decimal(f'{amount:.2f}')
        try:
            acc_code = AccountCode.get((AccountCode.account == acc) & (AccountCode.code == code))
            if acc_code.id == source_acc_code.id:
                return jsonify(result=False, message=strings.same_account_move), 400
            acc_code.balance = AccountCode.balance + amount
            acc_code.updated = datetime.now()
        except AccountCode.DoesNotExist:
            acc_code = AccountCode.create(account=acc, code=code, balance=amount)
        source_acc_code.balance = AccountCode.balance - amount
        source_acc_code.updated = datetime.now()
        source_acc_code.save()
        source_log = AccountLog.create(
            account=source_acc_code.account,
            account_code=source_acc_code,
            created=datetime.now(),
            type=AccountLog.TYPE_CREDIT,
            amount=amount,
            user=g.user,
            doc_number=number,
            doc_date=mysql_date(date)
        )
        dst_log = AccountLog.create(
            account=acc,
            account_code=acc_code,
            created=datetime.now(),
            type=AccountLog.TYPE_DEBIT,
            amount=amount,
            related_log=source_log,
            user=g.user,
            doc_number=number,
            doc_date=mysql_date(date)
        )
        source_log.related_log = dst_log
        source_log.save()
        acc_code.save()

    acc_code = AccountCode.get_by_id(acc_code.id)
    return jsonify(result=True, data={'balance': acc_code.balance})


@bp.route('/api/account.log.undo', methods=('POST',))
@authorized_api
@permissions(read=True, update=True)
@validate_params(Param('id', JSON, int))
def account_log_cancel(log_id):
    return _account_log_undoredo(log_id, True)


@bp.route('/api/account.log.redo', methods=('POST',))
@authorized_api
@permissions(read=True, update=True)
@validate_params(Param('id', JSON, int))
def account_log_redo(log_id):
    return _account_log_undoredo(log_id, False)


@bp.route('/api/account.log.update', methods=('POST',))
@authorized_api
@permissions(read=True, update=True)
@validate_params(
    Param('id', JSON, int),
    Param('amount', JSON, float),
    Param('date', JSON, str, False),
    Param('number', JSON, str, False))
def account_code_update(log_id, amount, doc_date, doc_number):
    try:
        log = AccountLog.get((AccountLog.id == log_id) & (AccountLog.state == AccountLog.STATE_ACTIVE))
    except AccountLog.DoesNotExist:
        return jsonify(status=False, message=strings.log_not_found), 404

    if log.payment:
        return jsonify(status=False, message=strings.log_not_found), 404

    with g.db.atomic():
        amount = Decimal(f'{amount:.2f}')
        amount_diff = log.amount - amount
        log.account_code.balance = AccountCode.balance - amount_diff
        log.account_code.updated = datetime.now()
        log.amount = amount
        log.doc_date = mysql_date(doc_date)
        log.doc_number = doc_number
        log.account_code.save()
        log.save()
    return jsonify(status=True)
