from flask import Blueprint, jsonify, request, g, Response
from flask_request_validator import GET, JSON, Param, validate_params
from functools import reduce
from datetime import datetime
from time import time
from io import BytesIO
from operator import and_
from decimal import Decimal
from peewee import JOIN, fn
from .payment import format_payments
from .. import strings
from ..helpers import authorized_api, permissions, validate_field, mysql_date
from ..models import Agreement, Executor, AgreementCode, AgreementAccount, Code, Payment, Account
import json
import math
import pyexcel_xlsx

bp = Blueprint('api_agreement', __name__)
REDIS_NUMBERS_SET = 'reserved-ag-numbers'


def _get_agreement_dict(agrmt, codes, payments, detailed=False):
    result = {
        'id': agrmt.id,
        'codes': {},
        'payments': {},
        'number': agrmt.number,
        'executor': {
            'name': agrmt.executor.name,
            'taxnum': agrmt.executor.taxnum,
            'id': agrmt.executor.id
        } if agrmt.executor else None,
        'date': agrmt.date.strftime('%d.%m.%Y') if agrmt.date else None,
        'due_date': agrmt.due_date.strftime('%d.%m.%Y') if agrmt.due_date else None,
        'valid_date': agrmt.valid_date.strftime('%d.%m.%Y') if agrmt.valid_date else None,
        'amount': agrmt.amount,
        'subject': agrmt.subject,
        'state': agrmt.state,
        'prozorro': agrmt.prozorro,
        'type': agrmt.type,
        'notes': agrmt.notes,
        'debit': 0,
        'credit': agrmt.amount
    }
    if codes:
        for agr_code in codes:
            result['codes'].setdefault(Code.type_aliases[agr_code.code.type], []).append({
                'id': agr_code.id,
                'code_id': agr_code.code_id,
                'code': agr_code.code.code,
                'description': agr_code.code.description,
                'amount': agr_code.amount
            })
    if payments:
        payments_dict = format_payments(payments, detailed)
        if payments_dict['debit'] > 0:
            result['credit'] -= payments_dict['debit']
        result.update(payments_dict)
    if detailed:
        agreement_accounts = (AgreementAccount
                              .select(AgreementAccount, Account)
                              .join(Account)
                              .where(AgreementAccount.agreement == agrmt.id))
        result['accounts'] = []
        for agr_acc in agreement_accounts:
            result['accounts'].append({
                'id': agr_acc.id,
                'account_id': agr_acc.account_id,
                'name': agr_acc.account.name,
                'number': agr_acc.account.number
            })
    return result


def _get_totals(search_filter, search_codes, limit):
    def _get_decimal(value):
        return Decimal(value) if value is not None else Decimal()
    if search_codes:
        agrmts_ids = (AgreementCode
                      .select(fn.Distinct(AgreementCode.agreement))
                      .join(Agreement)
                      .where(AgreementCode.code << search_codes)
                      .tuples())
        agrmts_ids = {row[0] for row in agrmts_ids}
        search_filter.append(Agreement.id << agrmts_ids)
    agrmt_sum = (Agreement
                 .select(fn.Sum(Agreement.amount), fn.Count(Agreement.id))
                 .where(reduce(and_, search_filter))
                 .tuples())
    sums = {'amount': _get_decimal(agrmt_sum[0][0])}
    search_filter.append(Payment.state == Payment.STATE_ACTIVE)
    pay_sum = (Payment
               .select(fn.Sum(Payment.amount))
               .join(Agreement)
               .where(reduce(and_, search_filter))
               .tuples())
    sums['debit'] = _get_decimal(pay_sum[0][0])
    sums['credit'] = sums['amount'] - sums['debit']
    return {'sums': sums, 'pageCount': math.ceil(agrmt_sum[0][1] / limit)}


def _update_relations(model, model_rel_field, rel_model, rel_field, new_id_set, rel_model_amounts=None):
    codes = {r[0] for r in rel_model.select(rel_field).where(model_rel_field == model).tuples()}
    rel_to_add = new_id_set.difference(codes)
    rel_to_del = codes.difference(new_id_set)
    if isinstance(rel_model_amounts, dict):
        amount_field = getattr(rel_model, 'amount')
        rel_model.insert_many(
            {(model.id, rel, rel_model_amounts.get(rel)) for rel in new_id_set},
            (model_rel_field, rel_field, amount_field)
        ).on_conflict(update={amount_field: fn.VALUES(amount_field)}).execute()
    else:
        if rel_to_add:
            rel_model.insert_many({(model.id, rel) for rel in rel_to_add}, (model_rel_field, rel_field)).execute()
    if rel_to_del:
        rel_model.delete() \
            .where(model_rel_field == model, rel_field << rel_to_del) \
            .execute()


@bp.route('/api/agreements', methods=('GET', 'POST'))
@authorized_api
@permissions(read=True)
def agreements(export=False):
    def_search_values = [Agreement.state << {Agreement.STATE_ACTIVE, Agreement.STATE_FULFILLED}]
    def_sort_fields = Agreement.last_update.desc(), Agreement.date.desc(), Agreement.id.desc()
    js = request.get_json()
    search_codes = {js[key] for key in {'kdk', 'kpkv', 'kekv'} if js.get(key)}
    search_values = []
    for key in {'dateEnd', 'dateStart', 'executor', 'state'}:
        if js.get(key):
            if key == 'dateEnd':
                search_values.append(Agreement.date <= mysql_date(js[key]))
            elif key == 'dateStart':
                search_values.append(Agreement.date >= mysql_date(js[key]))
            elif key in {'executor', 'state'}:
                if js[key] != '0':
                    search_values.append(getattr(Agreement, key) == js[key])

    sums_search_values = search_values.copy()
    sort_field = def_sort_fields
    if js.get('sort'):
        field, direction = js.get('sort').popitem()
        if direction in {'desc', 'asc'}:
            sort_field = getattr(getattr(Agreement, field), direction)(), Agreement.id.desc()

    agrmts_ids = None
    if search_codes:
        agrmts_ids = (AgreementCode
                      .select(fn.Distinct(AgreementCode.agreement), Agreement.date, Agreement.last_update, Agreement.id))
        if not export:
            agrmts_ids = agrmts_ids.paginate(js.get('page', 1), js.get('limit', 20))
        agrmts_ids = (agrmts_ids
                      .join(Agreement)
                      .where(AgreementCode.code << search_codes)
                      .order_by(*sort_field)
                      .paginate(js.get('page', 1), js.get('limit', 20))
                      .tuples())
        agrmts_ids = {row[0] for row in agrmts_ids}
        if not agrmts_ids:
            return jsonify(result=False, message=strings.agreements_not_found)
        search_values.append(Agreement.id << agrmts_ids)
    else:
        if not search_values:
            search_values = def_search_values
            sums_search_values = search_values.copy()

    agrmts = (Agreement
              .select(Agreement, Executor)
              .join(Executor, JOIN.LEFT_OUTER)
              .where(reduce(and_, search_values))
              .order_by(*sort_field))
    if agrmts_ids is None and not export:
        agrmts = agrmts.paginate(js.get('page', 1), js.get('limit', 20))

    if not agrmts.exists():
        return jsonify(result=False, message=strings.agreements_not_found)
    agrmts_ids = {agreement.id for agreement in agrmts}

    codes = AgreementCode.select(AgreementCode, Code).join(Code).where(AgreementCode.agreement << agrmts_ids)
    codes_by_agrmt = {}
    payments_by_agrmt = {}
    for agr_code in codes:
        codes_by_agrmt.setdefault(agr_code.agreement_id, []).append(agr_code)
    payments = Payment.select().where((Payment.agreement << agrmts_ids) & (Payment.state == Payment.STATE_ACTIVE))
    for payment in payments:
        payments_by_agrmt.setdefault(payment.agreement_id, []).append(payment)
    data = {'agreements': [], 'sums': {'amount': 0, 'credit': 0, 'debit': 0}}
    for agrmt in agrmts:
        agr_dict = _get_agreement_dict(agrmt, codes_by_agrmt.get(agrmt.id), payments_by_agrmt.get(agrmt.id))
        data['agreements'].append(agr_dict)
    data.update(_get_totals(sums_search_values, search_codes, js.get('limit', 20)))
    return jsonify(status=True, data=data)


@bp.route('/api/agreements.export', methods=('POST',))
@authorized_api
@permissions(read=True)
def agreements_export():
    result = json.loads(agreements(True).get_data())
    if not result['status']:
        return jsonify(result)
    data = [('КПКВ', 'КЕКВ', 'КДК', 'Виконавець', 'ЄДРПОУ', 'Номер', 'Дата', 'Діє до', 'Сума договору', 'Предмет',
            'Стан', 'Дебіт', 'Кредит', 'Prozorro', 'Примітки')]
    buffer = BytesIO()
    states = {1: 'діючий', 2: 'виконаний', 3: 'видалений'}
    for agrmt in result['data']['agreements']:
        data.append((
            '; '.join(map(lambda x: x['code'], agrmt['codes']['kpkv'])),
            '; '.join(map(lambda x: x['code'], agrmt['codes']['kekv'])),
            '; '.join(map(lambda x: x['code'], agrmt['codes']['kdk'])),
            agrmt['executor']['name'],
            agrmt['executor']['taxnum'],
            agrmt['number'],
            agrmt['date'],
            agrmt['valid_date'],
            agrmt['amount'],
            agrmt['subject'],
            states[agrmt['state']],
            agrmt['debit'],
            agrmt['credit'],
            agrmt['prozorro'],
            agrmt['notes']
        ))
    pyexcel_xlsx.save_data(buffer, {'Sheet 1': data})
    buffer.seek(0)
    fname = f"agreements_{datetime.now().strftime('%Y%m%d_%H%M%S')}.xlsx"
    return Response(
        buffer,
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        headers={'x-filename': fname}
    )


@bp.route('/api/agreement.save', methods=('POST',))
@authorized_api
@permissions(read=True, update=True, create=True, delete=True)
def agreement_save():
    required = {'amount', 'codes.kekv', 'codes.kdk', 'codes.kpkv', 'date', 'executor.name', 'executor.taxnum', 'number',
                'state', 'subject', 'type', 'accounts', 'due_date'}
    js = request.get_json()
    for field in required:
        if not validate_field(field, js):
            return jsonify(status=False, message=strings.field_validate_error)

    if js.get('id'):
        try:
            agreement = Agreement[js['id']]
        except Agreement.DoesNotExist:
            return jsonify(status=False, message=strings.agreement_not_found)
    else:
        agreement = Agreement()
        g.redis.zrem(REDIS_NUMBERS_SET, js['number'])

    if js['executor'].get('id'):
        try:
            executor = Executor[js['executor']['id']]
        except Executor.DoesNotExist:
            return jsonify(status=False, message=strings.executor_not_found)
    else:
        try:
            executor = Executor\
                .get(Executor.name == js['executor']['name'], Executor.taxnum == js['executor']['taxnum'])
        except Executor.DoesNotExist:
            executor = Executor.create(name=js['executor']['name'], taxnum=js['executor']['taxnum'])

    code_set = set()
    code_sums = {}
    for ctype in {'kekv', 'kdk', 'kpkv'}:
        code_set.update(js['codes'][ctype])
        if ctype in js['sums']:
            code_sums.update({int(k): v.get('amount', 0) for k, v in js['sums'][ctype].items()})
        else:
            code_sums.update({int(cid): 0 for cid in js['codes'][ctype]})
    for field in {'date', 'due_date', 'valid_date'}:
        js[field] = mysql_date(js[field])

    with g.db.atomic():
        agreement.executor = executor
        agreement.user = g.user
        for field in {'amount', 'date', 'number', 'state', 'subject', 'type', 'due_date', 'valid_date', 'notes', 'prozorro'}:
            setattr(agreement, field, js.get(field))
        agreement.save()
        _update_relations(
            agreement,
            AgreementCode.agreement,
            AgreementCode,
            AgreementCode.code,
            {int(code_id) for code_id in code_set},
            code_sums
        )
        _update_relations(
            agreement,
            AgreementAccount.agreement,
            AgreementAccount,
            AgreementAccount.account,
            {int(acc_id) for acc_id in js['accounts']}
        )

    return jsonify(status=True, data={'agreementId': agreement.id})


@bp.route('/api/agreement.get', methods=('GET',))
@authorized_api
@permissions(read=True)
@validate_params(Param('id', GET, int))
def agreement_get(aid):
    try:
        agr = Agreement[aid]
    except Agreement.DoesNotExist:
        return jsonify(status=False, message=strings.agreement_not_found), 404
    codes = AgreementCode.select(AgreementCode, Code).join(Code).where(AgreementCode.agreement == aid)
    payments = Payment.select().where((Payment.agreement == aid) & (Payment.state == Payment.STATE_ACTIVE))
    return jsonify(status=True, data=_get_agreement_dict(agr, codes, payments, True))


@bp.route('/api/agreement.generateNumber', methods=('POST',))
@authorized_api
@permissions(read=True)
@validate_params(Param('date', JSON, str))
def agreement_generate_number(date):
    work_date = datetime.strptime(date, '%d.%m.%Y')
    month_part = f'{work_date.month:02}'
    ag_numbers = Agreement.select(Agreement.number).where(
        (fn.Month(Agreement.date) == work_date.month) &
        (fn.Year(Agreement.date) == work_date.year) &
        (Agreement.number.regexp(r'^[0-9]{2}/' + f'{month_part}$'))
    )
    numbers = set()
    for number in ag_numbers.tuples():
        numbers.add(number[0])

    score_limit = int(time()) - 900
    if REDIS_NUMBERS_SET in g.redis:
        g.redis.zremrangebyscore(REDIS_NUMBERS_SET, '-inf', score_limit)
    for number in g.redis.zrangebyscore(REDIS_NUMBERS_SET, score_limit, '+inf'):
        number = number.decode('utf-8')
        if number.endswith(f'/{month_part}'):
            numbers.add(number)
    if len(numbers) == 0:
        new_number = f'01/{month_part}'
    else:
        numbers = list(numbers)
        numbers.sort()
        new_number = f'{int(numbers[-1][:2])+1:02}/{month_part}'
    g.redis.zadd(REDIS_NUMBERS_SET, {new_number: int(time())}, nx=True)

    return jsonify(status=True, data={'number': new_number})
