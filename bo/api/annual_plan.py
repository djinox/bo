from flask import Blueprint, jsonify
from ..helpers import authorized_api, permissions, mysql_date
from ..models import AnnualCodePlan, Code, AgreementCode, Agreement
from .. import strings
from functools import reduce
from peewee import fn
from operator import and_
from flask_request_validator import JSON, Param, validate_params

bp = Blueprint('api_annual_plan', __name__)


@bp.route('/api/plans', methods=('POST', 'GET'))
@authorized_api
@permissions(read=True)
@validate_params(
    Param('year', JSON, int, False),
    Param('code', JSON, int, False),
    Param('dateStart', JSON, str, False),
    Param('dateEnd', JSON, str, False)
)
def plans_get(year, code, date_start, date_end):
    where = []
    if year:
        where.append(AnnualCodePlan.year == year)
    if code:
        where.append(AnnualCodePlan.code == code)
    if date_start:
        where.append(AnnualCodePlan.date >= mysql_date(date_start))
    if date_end:
        where.append(AnnualCodePlan.date <= mysql_date(date_end))
    if where:
        where.append(AnnualCodePlan.state == AnnualCodePlan.STATE_ACTIVE)
        plans = (AnnualCodePlan
                 .select(AnnualCodePlan, Code)
                 .join(Code)
                 .where(reduce(and_, where))
                 .order_by(AnnualCodePlan.id.desc()))
    else:
        plans = (AnnualCodePlan
                 .select(AnnualCodePlan, Code)
                 .join(Code)
                 .order_by(AnnualCodePlan.id.desc())
                 .where(AnnualCodePlan.state == AnnualCodePlan.STATE_ACTIVE)
                 .limit(20))
    data = {'plans': [], 'sum': 0}
    if plans:
        for plan in plans:
            data['plans'].append({
                'id': plan.id,
                'year': plan.year,
                'code': {'code': plan.code.code, 'description': plan.code.description, 'id': plan.code_id},
                'subject': plan.subject,
                'date': plan.date.strftime('%d.%m.%Y'),
                'amount': plan.amount,
                'notes': plan.notes
            })
            data['sum'] += plan.amount
        return jsonify(status=True, data=data)
    else:
        return jsonify(status=False, message=strings.no_plans_found, data={'sum': 0})


@bp.route('/api/plan.save', methods=('POST',))
@authorized_api
@permissions(read=True, create=True, update=True)
@validate_params(
    Param('id', JSON, int, False),
    Param('year', JSON, int),
    Param('date', JSON, str),
    Param('code', JSON, int),
    Param('subject', JSON, str),
    Param('amount', JSON, float),
    Param('notes', JSON, str, False)
)
def plan_save(plan_id, year, plan_date, code, subject, amount, notes):
    if plan_id:
        try:
            plan = AnnualCodePlan[plan_id]
        except AnnualCodePlan.DoesNotExist:
            return jsonify(status=False, message=strings.plan_not_found)
    else:
        plan = AnnualCodePlan()
    plan.year = year
    plan.date = mysql_date(plan_date)
    plan.code = code
    plan.subject = subject
    plan.amount = amount
    if notes:
        plan.notes = notes
    plan.save()
    return jsonify(status=True, id=plan.id)


@bp.route('/api/plan.agreements', methods=('POST',))
@authorized_api
@permissions(read=True)
@validate_params(Param('year', JSON, int), Param('code', JSON, int))
def plan_agreements(year, code):
    agr_codes = (AgreementCode
                 .select(AgreementCode, Agreement.number, Agreement.valid_date, Agreement.date)
                 .join(Agreement)
                 .where((AgreementCode.code == code) & (fn.YEAR(Agreement.valid_date) == year)))
    data = {'agreements': [], 'sum': 0}
    for agr_code in agr_codes:
        data['agreements'].append({
            'number': agr_code.agreement.number,
            'valid_date': agr_code.agreement.valid_date.strftime('%d.%m.%Y'),
            'date': agr_code.agreement.date.strftime('%d.%m.%Y'),
            'amount': agr_code.amount
        })
        data['sum'] += agr_code.amount
    if not data['agreements']:
        return jsonify(status=False, message=strings.agreements_not_found, data={'sum': 0})
    return jsonify(status=True, data=data)


@bp.route('/api/plan.remove', methods=('POST',))
@authorized_api
@permissions(delete=True)
@validate_params(Param('id', JSON, int))
def plan_remove(plan_id):
    try:
        plan = AnnualCodePlan[plan_id]
    except AnnualCodePlan.DoesNotExist:
        return jsonify(status=False, message=strings.plan_not_found)
    plan.state = AnnualCodePlan.STATE_DELETED
    plan.save()
    return jsonify(status=True)
