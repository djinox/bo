from flask import Blueprint, jsonify
from flask_request_validator import JSON, Param, validate_params
from functools import reduce
from ..helpers import authorized_api, permissions
from ..models import Code
import operator

bp = Blueprint('api_code', __name__)


@bp.route('/api/codes', methods=('POST',))
@authorized_api
@permissions(read=True)
@validate_params(Param('type', JSON, int), Param('q', JSON, str, False))
def codes(ctype, query):
    where = [(Code.type == ctype)]
    if query and len(query) >= 2:
        where.append(((Code.code.startswith(query)) | (Code.description.contains(query))))
    codes_found = Code.select().where(reduce(operator.and_, where)).limit(8)
    data = []
    for code in codes_found:
        data.append({
            'id': code.id,
            'name': f'{code.code} {code.description}',
            'text': f'{code.code} {code.description}'
        })
    return jsonify(result=True, data=data)
