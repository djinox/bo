from flask import Blueprint, jsonify
from flask_request_validator import JSON, Param, validate_params
from ..helpers import authorized_api, permissions
from ..models import Executor

bp = Blueprint('api_executor', __name__)


@bp.route('/api/executors', methods=('POST',))
@authorized_api
@permissions(read=True)
@validate_params(Param('q', JSON, str))
def codes(query):
    executors = Executor.select()
    if query and len(query) >= 2:
        executors = executors.where((Executor.name.contains(query)) | (Executor.taxnum.contains(query)))
    executors = executors.limit(8)
    data = []
    for executor in executors:
        data.append({'id': executor.id, 'name': executor.name, 'taxnum': executor.taxnum})
    return jsonify(result=True, data=data)
