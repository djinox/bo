from flask import Blueprint, jsonify, request, session, g, url_for
from flask_request_validator import JSON, Param, validate_params
from werkzeug.security import check_password_hash
from datetime import datetime
from .. import strings
from ..helpers import authorized_api
from ..models import User

bp = Blueprint('api_login', __name__)


@bp.route('/api/auth', methods=('POST',))
@validate_params(Param('login', JSON, str), Param('password', JSON, str))
def auth(login, password):
    user = User.get_or_none((User.login == login) & (User.state == User.STATE_ACTIVE))
    if user is None:
        return jsonify(result=False, message=strings.login_not_found), 404

    if not check_password_hash(user.password, password):
        return jsonify(result=False, message=strings.wrong_password), 403

    session.clear()
    session['id'] = user.id
    session['ip'] = request.remote_addr
    user.last_auth = datetime.now()
    user.last_ip = request.remote_addr
    user.save()

    return jsonify(result=True, redirect_path=url_for('entrypoints.admin'))


@bp.route('/api/me', methods=('GET',))
@authorized_api
def me():
    return jsonify(name=g.user.name)
