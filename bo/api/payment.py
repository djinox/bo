from flask import Blueprint, jsonify, g, request
from flask_request_validator import JSON, GET, Param, validate_params
from datetime import datetime
from peewee import fn
from decimal import Decimal
from ..helpers import authorized_api, permissions, validate_field, mysql_date, code_sums_to_decimal
from ..models import Payment, Agreement, PaymentCode, PaymentAccount, AgreementCode, AccountCode, AccountLog, Code
from ..models import Account
from .. import strings

bp = Blueprint('api_payment', __name__)


class PaymentCodeSumGreaterThanAgreementError(Exception):
    def __init__(self, code_id, asum):
        self.code_id = code_id
        self.asum = asum


class PaymentAmountGreaterThanAgreementError(Exception):
    pass


class PaymentCodeSumGreaterThanPaymentError(PaymentCodeSumGreaterThanAgreementError):
    pass


class TooMuchAccountsAndCodes(Exception):
    pass


def format_payments(payments, detailed=False):
    result = {'payments': {}, 'debit': 0}
    if detailed:
        payment_ids = {payment.id for payment in payments}
        codes_by_pmt = {}
        accounts_by_pmt = {}
        payment_codes = (PaymentCode
                         .select(PaymentCode, Code)
                         .join(Code)
                         .where(PaymentCode.payment << payment_ids))
        payment_accounts = (PaymentAccount
                            .select(PaymentAccount, Account)
                            .join(Account)
                            .where(PaymentAccount.payment << payment_ids))
        for pcode in payment_codes:
            codes_by_pmt.setdefault(pcode.payment_id, []).append(pcode)
        for pacc in payment_accounts:
            accounts_by_pmt.setdefault(pacc.payment_id, []).append(pacc)
        del payment_codes, payment_accounts
    for payment in payments:
        payment_data = {'id': payment.id, 'amount': payment.amount}
        if detailed:
            payment_data['date'] = payment.date.strftime('%d.%m.%Y')
            payment_data['number'] = payment.number
            payment_data['comment'] = payment.comment
            payment_data['codes'] = {}
            payment_data['accounts'] = []
            for pcode in codes_by_pmt.get(payment.id):
                payment_data['codes'].setdefault(Code.type_aliases[pcode.code.type], []).append({
                    'id': pcode.id,
                    'code_id': pcode.code_id,
                    'code': pcode.code.code,
                    'description': pcode.code.description,
                    'amount': pcode.amount
                })
            for pacc in accounts_by_pmt.get(payment.id):
                payment_data['accounts'].append({
                    'id': pacc.id,
                    'account_id': pacc.account_id,
                    'name': pacc.account.name,
                    'number': pacc.account.number,
                    'amount': pacc.amount
                })
        result['payments'][payment.id] = payment_data
        result['debit'] += payment.amount
    return result


def _update_account_balances(acc_sums, code_sums, payment):
    def _do_update(codeid, accid, acc_code_sum):
        if acc_code_sum != 0:
            try:
                acc_code = AccountCode.get((AccountCode.code == codeid) & (AccountCode.account == accid))
                acc_code.balance = AccountCode.balance - acc_code_sum
                acc_code.save()
            except AccountCode.DoesNotExist:
                acc_code = AccountCode.create(account=accid, code=codeid, balance=-acc_code_sum)
            AccountLog.create(
                account=accid,
                account_code=acc_code,
                amount=abs(acc_code_sum),
                created=datetime.now(),
                type=AccountLog.TYPE_CREDIT if acc_code_sum > 0 else AccountLog.TYPE_DEBIT,
                payment=payment,
                user=g.user,
                doc_date=payment.date,
                doc_number=payment.number
            )

    acc_count = len(acc_sums.keys())
    code_count = len(code_sums.keys())
    if acc_count == 0 and code_count == 0:
        return True

    if acc_count > 1 and code_count > 1:
        raise TooMuchAccountsAndCodes
    if acc_count > 1:
        code_id = [*code_sums.keys()][0]
        for acc_id, acc_sum in acc_sums.items():
            _do_update(code_id, acc_id, acc_sums[acc_id])
    elif code_count > 1:
        acc_id = [*acc_sums.keys()][0]
        for code_id, code_sum in code_sums.items():
            _do_update(code_id, acc_id, code_sums[code_id])
    else:
        code_id = [*code_sums.keys()][0]
        acc_id = [*acc_sums.keys()][0]
        _do_update(code_id, acc_id, code_sums[code_id])


def _validate_payment_sums(agreement, new_code_sums, amount, payment_to_update=None):
    for code, psum in new_code_sums.items():
        if psum > amount:
            raise PaymentCodeSumGreaterThanPaymentError(code, psum)
    agr_codes = (AgreementCode
                 .select(AgreementCode.code_id, AgreementCode.amount)
                 .where(AgreementCode.agreement == agreement)
                 .tuples())
    agr_sums = {code[0]: code[1] for code in agr_codes}
    payments = Payment.select().where((Payment.agreement == agreement) & (Payment.state == Payment.STATE_ACTIVE))
    payment_ids = set()
    payments_sum = Decimal()
    for pmt in payments:
        payment_ids.add(pmt.id)
        if payment_to_update:
            payments_sum += amount if payment_to_update.id == pmt.id else pmt.amount
        else:
            payments_sum += pmt.amount

    if payments_sum > agreement.amount:
        raise PaymentAmountGreaterThanAgreementError

    pmt_sums = {}
    if payment_to_update and payment_ids:
        payment_ids.remove(payment_to_update.id)
    if payment_ids:
        pmt_codes = (PaymentCode
                     .select(PaymentCode.code_id, fn.SUM(PaymentCode.amount))
                     .where(PaymentCode.payment << payment_ids)
                     .group_by(PaymentCode.code_id)
                     .tuples())
        pmt_sums = {code[0]: Decimal(code[1]) for code in pmt_codes}

    for acode_id, asum in agr_sums.items():
        if asum and pmt_sums.get(acode_id, 0)+new_code_sums.get(acode_id, 0) > asum:
            raise PaymentCodeSumGreaterThanAgreementError(acode_id, asum)

    return True


@bp.route('/api/payment.update', methods=('POST',))
@authorized_api
@permissions(read=True, update=True)
@validate_params(
    Param('id', JSON, int),
    Param('amount', JSON, float),
    Param('date', JSON, str),
    Param('number', JSON, str),
    Param('comment', JSON, str, False)
)
def payment_update(payment_id, amount, date, number, comment):
    js = request.get_json()
    amount = Decimal(f'{amount:.2f}')
    for field in {'sums.kekv', 'sums.kpkv', 'sums.account'}:
        if not validate_field(field, js):
            return jsonify(status=False, message=strings.field_validate_error)
    try:
        payment = Payment[payment_id]
    except Payment.DoesNotExist:
        return jsonify(status=False, message=strings.payment_not_found)

    js['sums']['account'] = code_sums_to_decimal(js['sums']['account'])

    try:
        payment_code_sums = {}
        for ctype in {'kekv', 'kpkv'}:
            payment_code_sums.update(code_sums_to_decimal(js['sums'][ctype]))
        _validate_payment_sums(payment.agreement, payment_code_sums, amount, payment)
    except PaymentAmountGreaterThanAgreementError:
        return jsonify(status=False, message=strings.payment_sum_is_greater)
    except PaymentCodeSumGreaterThanPaymentError as e:
        code = Code[e.code_id]
        return jsonify(
            status=False,
            message=strings.payment_code_sum_greater_than_pamount.format(code.code, code.description, e.asum)
        )
    except PaymentCodeSumGreaterThanAgreementError as e:
        code = Code[e.code_id]
        return jsonify(
            status=False,
            message=strings.payment_code_sum_is_greater.format(code.code, code.description, e.asum))

    with g.db.atomic():
        payment.amount = amount
        payment.date = mysql_date(date)
        payment.number = number
        if comment:
            payment.comment = comment

        kekv_diffs = {}
        for pcode in payment.codes:
            for code_id in list(payment_code_sums.keys()):
                if pcode.code_id == code_id:
                    code_sum = payment_code_sums[code_id]
                    if pcode.code.type == Code.TYPE_KEKV:
                        kekv_diffs[code_id] = code_sum - pcode.amount
                    if pcode.amount != code_sum:
                        pcode.amount = code_sum
                        pcode.save()
                    del payment_code_sums[code_id]
                    break
        if len(payment_code_sums) > 0:
            PaymentCode.insert_many(
                {(payment, code, amount) for code, amount in payment_code_sums.items()},
                (PaymentCode.payment, PaymentCode.code, PaymentCode.amount)
            ).execute()
            for code_id, code_sum in payment_code_sums.items():
                kekv_diffs[code_id] = code_sum

        account_diffs = {}
        for pacc in payment.accounts:
            for acc_id in list(js['sums']['account'].keys()):
                if pacc.account_id == acc_id:
                    acc_sum = js['sums']['account'][acc_id]
                    account_diffs[acc_id] = acc_sum - pacc.amount
                    if pacc.amount != acc_sum:
                        pacc.amount = acc_sum
                        pacc.save()
                    del js['sums']['account'][acc_id]
                    break
        if len(js['sums']['account']) > 0:
            PaymentAccount.insert_many(
                {(payment, account, amount) for account, amount in js['sums']['account'].items()},
                (PaymentAccount.payment, PaymentAccount.account, PaymentAccount.amount)
            ).execute()
            for acc_id, acc_sum in js['sums']['account'].items():
                account_diffs[acc_id] = acc_sum

        _update_account_balances(account_diffs, kekv_diffs, payment)
        payment.save()
    return jsonify(status=True)


@bp.route('/api/payment.add', methods=('POST',))
@authorized_api
@permissions(read=True)
@validate_params(
    Param('agreementId', JSON, int),
    Param('number', JSON, str),
    Param('date', JSON, str),
    Param('amount', JSON, float),
    Param('comment', JSON, str, False)
)
def payment_add(aid, number, date, amount, comment):
    js = request.get_json()
    amount = Decimal(f'{amount:.2f}')
    for field in {'sums.account', 'sums.kekv', 'sums.kpkv'}:
        if not validate_field(field, js):
            return jsonify(status=False, message=strings.field_validate_error)
    try:
        agreement = Agreement[aid]
    except Agreement.DoesNotExist:
        return jsonify(status=False, message=strings.agreement_not_found)

    payment_code_sums = {}
    for ctype in {'kekv', 'kpkv'}:
        payment_code_sums.update(code_sums_to_decimal(js['sums'][ctype]))
    try:
        _validate_payment_sums(agreement, payment_code_sums, amount)
    except PaymentAmountGreaterThanAgreementError:
        return jsonify(status=False, message=strings.payment_sum_is_greater)
    except PaymentCodeSumGreaterThanAgreementError as e:
        code = Code[e.code_id]
        return jsonify(
            status=False,
            message=strings.payment_code_sum_is_greater.format(code.code, code.description, e.asum)
        )

    js['sums']['account'] = code_sums_to_decimal(js['sums']['account'])
    payment_data = {
        'agreement': agreement,
        'number': number,
        'amount': amount,
        'date': mysql_date(date),
        'user': g.user,
        'date_create': datetime.now().date()
    }
    if comment:
        payment_data['comment'] = comment
    with g.db.atomic():
        payment = Payment.create(**payment_data)
        PaymentCode.insert_many(
            {(payment, code, amount) for code, amount in payment_code_sums.items()},
            (PaymentCode.payment, PaymentCode.code, PaymentCode.amount)
        ).execute()
        PaymentAccount.insert_many(
            {(payment, account, amount) for account, amount in js['sums']['account'].items()},
            (PaymentAccount.payment, PaymentAccount.account, PaymentAccount.amount)
        ).execute()
        _update_account_balances(js['sums']['account'], js['sums']['kekv'], payment)

    return jsonify(status=True, data={'paymentId': payment.id})


@bp.route('/api/payment.remove', methods=('POST',))
@authorized_api
@permissions(delete=True)
@validate_params(Param('id', JSON, int))
def payment_remove(payment_id):
    try:
        payment = Payment[payment_id]
    except Payment.DoesNotExist:
        return jsonify(status=False, message=strings.payment_not_found)
    if payment.state == Payment.STATE_DELETED:
        return jsonify(status=False, message=strings.payment_already_deleted)
    payment.state = Payment.STATE_DELETED
    kekv_codes = (PaymentCode
                  .select(PaymentCode.code, PaymentCode.amount)
                  .join(Code)
                  .where((PaymentCode.payment == payment) & (Code.type == Code.TYPE_KEKV))
                  .tuples())
    payment_accounts = (PaymentAccount
                        .select(PaymentAccount.account, PaymentAccount.amount)
                        .where(PaymentAccount.payment == payment)
                        .tuples())
    kekv_sums = {int(pc[0]): -pc[1] for pc in kekv_codes}
    account_sums = {int(pa[0]): -pa[1] for pa in payment_accounts}
    with g.db.atomic():
        _update_account_balances(account_sums, kekv_sums, payment)
        payment.save()
    return jsonify(status=True)


@bp.route('/api/payments.get', methods=('GET',))
@authorized_api
@permissions(read=True)
@validate_params(Param('agreementId', GET, int))
def payments_get(aid):
    try:
        agreement = Agreement[aid]
    except Agreement.DoesNotExist:
        return jsonify(status=False, message=strings.agreement_not_found)
    payments = Payment.select().where((Payment.agreement == aid) & (Payment.state == Payment.STATE_ACTIVE))
    result = format_payments(payments, True)
    result['credit'] = agreement.amount - result['debit']
    return jsonify(status=True, data=result)
