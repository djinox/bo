from flask import current_app, g
from .models import db_link


def init_db():
    if 'db' not in g:
        db_conf = {
            'database': current_app.config['DATABASE']['name'],
            'user': current_app.config['DATABASE']['user'],
            'password': current_app.config['DATABASE']['password']
        }
        if 'unix_socket' in current_app.config['DATABASE']:
            db_conf['unix_socket'] = current_app.config['DATABASE']['unix_socket']
        else:
            db_conf['host'] = current_app.config['DATABASE']['host']
        db_link.init(**db_conf)
        db_link.connection()
        g.db = db_link


def close_db(e=None):
    db = g.pop('db', None)
    if db and not db.is_closed():
        db.close()


def init_app(app):
    app.before_request(init_db)
    app.teardown_appcontext(close_db)
