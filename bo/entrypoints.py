from flask import Blueprint, redirect, render_template, url_for, g, session

bp = Blueprint('entrypoints', __name__)


@bp.route('/', methods=('GET',))
def index():
    return redirect(url_for('.admin' if g.user else '.login'))


@bp.route('/cp/', methods=('GET',))
@bp.route('/cp/<path:url>', methods=('GET',))
def admin(url=None):
    if g.user is None:
        return redirect(url_for('.login'))
    return render_template('admin.html')


@bp.route('/login', methods=('GET',))
def login():
    if g.user is not None:
        return redirect(url_for('.admin'))
    return render_template('login.html')


@bp.route('/logout', methods=('GET',))
def logout():
    session.clear()
    return redirect(url_for('.login'))
