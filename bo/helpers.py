from flask import session, request, g, jsonify
from functools import wraps
from .models import User
from datetime import datetime
from decimal import Decimal


def init_user():
    if 'user' not in g:
        if 'id' in session and 'ip' in session and request.remote_addr == session['ip']:
            g.user = User.get_or_none((User.id == session['id']) & (User.state == User.STATE_ACTIVE))
        else:
            g.user = None


def unauthorized():
    return jsonify(status=False, message='Unauthorized'), 403


def authorized_api(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        if g.user is None:
            return unauthorized()
        return func(*args, **kwargs)
    return wrapped


def permissions(read=None, create=None, update=None, delete=None):
    def wrapped_func(func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            if (
                    (read and read != g.user.perm_read)
                    or (create and create != g.user.perm_create)
                    or (update and update != g.user.perm_update)
                    or (delete and delete != g.user.perm_delete)
            ):
                return unauthorized()
            return func(*args, **kwargs)
        return wrapped
    return wrapped_func


def validate_field(field, where):
    if '.' not in field:
        return field in where and bool(where[field])
    else:
        parts = field.split('.', 1)
        if parts[0] not in where or not bool(where[parts[0]]):
            return False
        return validate_field(parts[1], where[parts[0]])


def mysql_date(date):
    return datetime.strptime(date, '%d.%m.%Y').strftime('%Y-%m-%d')


def code_sums_to_decimal(sums):
    return {int(k): Decimal(f'{v:.2f}') for k, v in sums.items()}