from peewee import MySQLDatabase, DecimalField, Model, PrimaryKeyField, CharField, DateTimeField, IPField
from peewee import FixedCharField, BitField, SmallIntegerField, ForeignKeyField, DateField, TextField
from functools import partial

db_link = MySQLDatabase(None)
MoneyField = partial(DecimalField, decimal_places=2, null=False)


class IModel(Model):
    id = PrimaryKeyField()

    class Meta:
        database = db_link
        legacy_table_names = False
        only_save_dirty = True


class User(IModel):
    STATE_INACTIVE = 0
    STATE_ACTIVE = 1

    login = CharField(max_length=32, unique=True)
    password = FixedCharField(max_length=93)
    name = CharField(max_length=32)
    last_auth = DateTimeField()
    last_ip = IPField()
    permissions = BitField()
    perm_read = permissions.flag(1)
    perm_create = permissions.flag(2)
    perm_update = permissions.flag(4)
    perm_delete = permissions.flag(8)
    state = SmallIntegerField(default=STATE_ACTIVE)


class Account(IModel):
    TYPE_ODEKU = 1
    TYPE_OKKT = 2
    TYPE_ND4 = 3

    type_aliases = {TYPE_ODEKU: 'odeku', TYPE_OKKT: 'okkt', TYPE_ND4: 'nd4'}

    balance = MoneyField()
    number = CharField(max_length=29, null=False)
    name = CharField(max_length=64, null=False)
    type = SmallIntegerField(null=False)
    updated = DateTimeField(null=True)
    current = SmallIntegerField(default=0)
    year = SmallIntegerField(null=False)


class Executor(IModel):
    name = CharField(null=False)
    taxnum = CharField(null=False)


class Code(IModel):
    TYPE_KEKV = 1
    TYPE_KDK = 2
    TYPE_KPKV = 3

    type_aliases = {TYPE_KEKV: 'kekv', TYPE_KDK: 'kdk', TYPE_KPKV: 'kpkv'}

    type = SmallIntegerField(null=False)
    code = CharField(max_length=25, null=False)
    description = CharField(max_length=100, null=False)
    parent_code = ForeignKeyField('self', default=0)


class AccountCode(IModel):
    code = ForeignKeyField(Code, backref='accounts', null=False)
    account = ForeignKeyField(Account, backref='codes', null=False)
    balance = MoneyField()

    class Meta:
        indexes = (
            (('code', 'account'), True)
        )


class Agreement(IModel):
    TYPE_DEFAULT = 1
    TYPE_TENDER = 2

    STATE_ACTIVE = 1
    STATE_FULFILLED = 2
    STATE_DELETED = 3

    date = DateField(null=False)
    due_date = DateField(null=True)
    valid_date = DateField(null=True)
    number = CharField(max_length=32)
    type = SmallIntegerField(default=TYPE_DEFAULT, null=False)
    executor = ForeignKeyField(Executor, null=False, backref='agreements')
    subject = CharField(null=False)
    amount = MoneyField()
    state = SmallIntegerField(null=False, default=STATE_ACTIVE)
    user = ForeignKeyField(User, null=False, backref='agreements')
    notes = TextField(null=True)
    prozorro = SmallIntegerField(default=0, null=False)
    last_update = DateTimeField(null=True)


class AdditionalAgreement(IModel):
    agreement = ForeignKeyField(Agreement, backref='add_agreements', null=False)
    date = DateField(null=False)
    amount = MoneyField()


class Payment(IModel):
    STATE_ACTIVE = 1
    STATE_DELETED = 0

    agreement = ForeignKeyField(Agreement, backref='payments', null=False)
    amount = MoneyField()
    number = CharField(max_length=128, null=False)
    date = DateField(null=False)
    date_create = DateField(null=False)
    user = ForeignKeyField(User, backref='payments', null=False)
    comment = CharField(null=True)
    state = SmallIntegerField(default=STATE_ACTIVE, null=False)


class PaymentCode(IModel):
    payment = ForeignKeyField(Payment, backref='codes', null=False)
    code = ForeignKeyField(Code, backref='payments', null=False)
    amount = MoneyField()

    class Meta:
        indexes = (
            (('code', 'payment'), True)
        )


class PaymentAccount(IModel):
    payment = ForeignKeyField(Payment, backref='accounts', null=False)
    account = ForeignKeyField(Account, backref='payments', null=False)
    amount = MoneyField()

    class Meta:
        indexes = (
            (('account', 'payment'), True)
        )


class AgreementCode(IModel):
    code = ForeignKeyField(Code, backref='agreements', null=False)
    agreement = ForeignKeyField(Agreement, backref='codes', null=False)
    amount = MoneyField()

    class Meta:
        indexes = (
            (('code', 'agreement'), True)
        )


class AgreementAccount(IModel):
    account = ForeignKeyField(Account, backref='agreements', null=False)
    agreement = ForeignKeyField(Agreement, backref='codes', null=False)

    class Meta:
        indexes = (
            (('account', 'agreement'), True)
        )


class AccountLog(IModel):
    TYPE_CREDIT = 1
    TYPE_DEBIT = 2
    
    STATE_ACTIVE = 1
    STATE_CANCELED = 2

    account = ForeignKeyField(Account, backref='logs', null=False)
    account_code = ForeignKeyField(AccountCode, null=False)
    doc_number = CharField(max_length=128)
    doc_date = DateField()
    related_log = ForeignKeyField('self', null=True)
    amount = MoneyField()
    created = DateTimeField()
    type = SmallIntegerField(null=False)
    payment = ForeignKeyField(Payment, backref='logs', null=True)
    user = ForeignKeyField(User, backref='account_log', null=False)
    state = SmallIntegerField(null=False, default=STATE_ACTIVE)


class AnnualCodePlan(IModel):
    STATE_ACTIVE = 1
    STATE_DELETED = 0

    year = SmallIntegerField(null=False)
    date = DateField()
    code = ForeignKeyField(Code, backref='plans', null=False)
    subject = CharField(null=False)
    amount = MoneyField()
    notes = CharField(null=True)
    state = SmallIntegerField(default=STATE_ACTIVE, null=False)
