let app = angular.module('bo', ['ngRoute', 'ngAnimate', 'toastr']);
app.config(function($routeProvider, $locationProvider, $interpolateProvider, $compileProvider, toastrConfig) {
    if (/bo-odeku\.ga/i.test(window.location.hostname)) {
        $compileProvider.debugInfoEnabled(false);
    }
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
    $routeProvider
    .when('/', {
        templateUrl: '/static/ng/admin/partials/home.html'
    })
    .when('/accounts', {
        templateUrl: '/static/ng/admin/partials/accounts.html',
        controller: 'AccountsCtrl'
    })
    .when('/accounts/:accountId', {
        templateUrl: '/static/ng/admin/partials/account.html',
        controller: 'AccountCtrl'
    })
    .when('/agreements', {
        templateUrl: '/static/ng/admin/partials/agreements.html',
        controller: 'AgreementsCtrl'
    })
    .when('/agreement/:agreementId', {
        templateUrl: '/static/ng/admin/partials/agreement.html',
        controller: 'AgreementCtrl'
    })
    .when('/reports', {
        templateUrl: '/static/ng/admin/partials/dk-reports.html',
        controller: 'DKReportsCtrl'
    })
    .otherwise({
        redirectTo: '/'
    });
    $locationProvider.html5Mode(true);
    angular.extend(toastrConfig, {positionClass: 'toast-bottom-right'});
});