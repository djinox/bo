app
.controller('NavCtrl', ['$scope', '$http', '$window',
    function NavCtrl($scope, $http, $window) {
        $scope.loaded = false;
        $scope.logout = function() {
            $window.location.href = '/logout';
        };
        $http.get('/api/me').then(function success(r) {
            $scope.user = r.data.name;
            $scope.loaded = true;
        }, function error(r) {
            console.error(r);
        });
    }
])
.controller('AccountsCtrl', ['$scope', '$http', 'Page',
    function AccountsCtrl($scope, $http, Page) {
        Page.setTitle('Рахунки');

        const MIN_YEAR = 2019;
        $scope.years = [];
        for (let i = (new Date).getFullYear(); i >= MIN_YEAR; i--) {
            $scope.years.push(i);
        }
        $scope.year = $scope.years[0];

        let _fillTypeCodes = function(data) {
            $scope.typeCodes = {};
            for (let key in data.accounts) {
                $scope.typeCodes[key] = new Set();
                for (let i in data.accounts[key]) {
                    for (let k in data.accounts[key][i].codes) {
                        $scope.typeCodes[key].add(k);
                    }
                }
                $scope.typeCodes[key] = [...$scope.typeCodes[key]];
            }
        };

        $scope.isEmptyObject = function(obj) {
            return Object.keys(obj).length === 0;
        };

        $scope.loadAccounts = function() {
            $http.get('/api/accounts', {params: {year: $scope.year}}).then(function success(r) {
                if (r.data.result) {
                    _fillTypeCodes(r.data.data);
                    $scope.data = r.data.data;
                } else {
                    console.warn(r.data);
                }
            }, function error(r) {
                console.error(r);
            });
        };

        $scope.loadAccounts();
    }
])
.controller('AccountCtrl', ['$scope', '$http', '$routeParams', 'Page', 'toastr', 'ErrorResponse',
    function AccountCtrl($scope, $http, $routeParams, Page, toastr, ErrorResponse) {
        /* add funds form logic */
        $scope.addForm = {id: $routeParams.accountId};
        $scope.logsRpp = 20;
        $scope.setAFCode = function(code) {
            if (code === null) {
                delete $scope.addForm.code;
                return;
            }

            $scope.addForm.code = code.id;
        };
        $scope.afSubmit = function() {
            $http.post('/api/account.addfunds', $scope.addForm).then(function success(r) {
                $scope._handleFundsResponse(r, 'af');
            }, function error(r) {
                $scope._handleErrorResponse(r, 'af');
            });
        };

        /* move funds form logic */
        $scope.moveForm = {id: $routeParams.accountId};
        $scope.setMFCode = function(code) {
            if (code === null) {
                delete $scope.moveForm.code;
                return;
            }

            $scope.moveForm.code = code.id;
            $scope.$apply($scope.mfValidateSameAccountCode);
        };
        $scope.setMFAccount = function(account) {
            if (account === null) {
                delete $scope.mfSourceAccount;
                return;
            }

            $scope.mfSourceAccount = account.id;
            $http.post('/api/account.code.search', {accountId: account.id}).then(function(r) {
                if (r.data.result) {
                    jQuery('#inputKekv3').data('typeahead').setSource(r.data.data);
                    $scope.mfAccountCode = '';
                    $scope.moveForm.fromAccountCode = null;
                }
            });
            $scope.$apply($scope.mfValidateSameAccountCode);
        };
        $scope.setMFAccountCode = function(acc_code) {
            if (acc_code === null) {
                delete $scope.moveForm.fromAccountCode;
                return;
            }

            $scope.moveForm.fromAccountCode = acc_code.id;
            $scope.mfSourceCode = acc_code.code;
            $scope.$apply($scope.mfValidateSameAccountCode);
        };
        $scope.mfSubmit = function() {
            if (!$scope.mfValidateSameAccountCode()) {
                return;
            }

            $http.post('/api/account.movefunds', $scope.moveForm).then(function(r) {
                $scope._handleFundsResponse(r, 'mf');
                $scope.mfAccount = '';
                $scope.mfAccountCode = '';
            }, function(r) {
                $scope._handleErrorResponse(r, 'mf');
            });
        };
        $scope.mfValidateSameAccountCode = function() {
            let valid = !(
                $scope.moveForm.code 
                && $scope.moveForm.id == $scope.mfSourceAccount
                && $scope.moveForm.code == $scope.mfSourceCode
            );
            $scope.mf.mfAccountCode.$setValidity('sameAcc', valid);
            return valid;
        };

        /* handling responses */
        $scope._handleFundsResponse = function(r, prefix) {
            if (r.data.result) {
                $scope[prefix + 'Error'] = false;
                $scope[prefix + 'Balance'] = r.data.data.balance;
                $scope[prefix + 'Success'] = true;
                $scope[prefix + 'Code'] = '';
                $scope[(prefix === 'af' ? 'addForm' : 'moveForm')] = {id: $routeParams.accountId};
                $scope.loadAccount(true);
            }
        };
        $scope._handleErrorResponse = function(r, prefix) {
            if (r.data && r.data.hasOwnProperty('message')) {
                $scope[prefix + 'Error'] = r.data.message;
                return;
            }

            if (r.status && r.statusText) {
                $scope[prefix + 'Error'] = r.status + ' (' + r.statusText + ')';
                return;
            }

            $scope[prefix + 'Error'] = 'Проблема с мережевим підключенням';
        };

        /* account loading */
        $scope.loadAccount = function(reload) {
            $http.get('/api/account.get', {params: {id: $routeParams.accountId}}).then(
                function success(r) {
                    if (r.data.result) {
                        $scope.data = r.data.data;
                        if (!reload) {
                            Page.setTitle('Рахунок ' + $scope.data.number + ' - ' + $scope.data.name);
                        }
                    } else {
                        console.warn(r.data);
                    }
                }, function error(r) {
                    ErrorResponse.handle(r, $scope);
                }
            );
            $scope.loadLogs(1);
            $scope.$broadcast('resetPage');
        };

        $scope.loadLogs = function(page) {
            $http.get(
                '/api/account.logs.get',
                {params: {id: $routeParams.accountId, page: page, limit: $scope.logsRpp}}
            ).then(
                function success(r) {
                    if (r.data.result) {
                        $scope.dataLogs = r.data.data;
                        $scope.dataLogs.curPage = page - 1;
                    }
                }, function error(r) {
                    ErrorResponse.handle(r, $scope);
                }
            );
        };

        $scope.setLogsPageCb = function(e, page) {
            $scope.loadLogs(page);
        };

        $scope.cancelOperation = function() {
            $http.post('/api/account.log.undo', {id: jQuery(this).data('id')}).then(function(r) {
                toastr.success('Операцію відмінено успішно!');
                $scope.loadAccount(true);
            }, function error(r) {
                ErrorResponse.handle(r, $scope);
            });
        };

        $scope.redoOperation = function() {
            $http.post('/api/account.log.redo', {id: jQuery(this).data('id')}).then(function(r) {
                toastr.success('Операцію відновлено успішно!');
                $scope.loadAccount(true);
            }, function error(r) {
                ErrorResponse.handle(r, $scope);
            });
        };

        /* log edit form logic */
        $scope.logEditForm = {};
        $scope.openLogEditPopup = function(logId) {
            let logObj = _.find($scope.data.logs, {id: logId});
            $scope.logEditCode = logObj.account_code.code;
            $scope.logEditDesc = logObj.account_code.description;
            $scope.logEditForm.id = logId;
            $scope.logEditForm.amount = logObj.amount;
            $scope.logEditForm.date = logObj.doc_date;
            $scope.logEditForm.number = logObj.doc_number;
            jQuery('#logEditForm').modal('toggle');
        };

        $scope.updateLog = function() {
            $scope.processing = true;
            $http.post('/api/account.log.update', $scope.logEditForm).then(function success(r) {
                $scope.processing = false;
                jQuery('#logEditForm').modal('toggle');
                $scope.loadAccount(true);
                toastr.success('Операцію збережено успішно!');
            }, function error(r) {
                ErrorResponse.handle(r, $scope);
            });
        };

        $scope.loadAccount();
    }
])
.controller('AgreementsCtrl', ['$scope', '$http', '$location', '$window', 'Page', 'ErrorResponse', 'toastr',
    function AgreementsCtrl($scope, $http, $location, $window, Page, ErrorResponse, toastr) {
        Page.setTitle('Договори');
        $scope.filterForm = {state: '0', page: 1, limit: 20};

        $scope.resetPage = function() {
            $scope.filterForm.page = 1;
            $scope.$broadcast('resetPage');
        };

        $scope.loadAgreements = function(resetPage) {
            if (resetPage) {
                $scope.resetPage();
            }

            $http.post('/api/agreements', $scope.filterForm).then(function success(r) {
                $scope.data = r.data;
            }, function error(r) {
                ErrorResponse.handle(r, $scope);
            });
        };

        $scope.setDateStart = function(date) {
            jQuery('#iDateEnd').datepicker('setStartDate', date);
        };
        $scope.setDateEnd = function(date) {
            jQuery('#iDateStart').datepicker('setEndDate', date);
        };
        $scope.setCode = function(item, attrs) {
            if (item === null) {
                delete $scope.filterForm[attrs.codeType];
                return;
            }

            $scope.filterForm[attrs.codeType] = item.id;
        };
        $scope.setExecutor = function(item) {
            if (item === null) {
                delete $scope.filterForm.executor;
                return;
            }

            $scope.filterForm.executor = item.id;
        };
        $scope.applySorting = function(field, direction) {
            _.set($scope.filterForm, ['sort', field], direction);
            $scope.resetPage();
            $scope.loadAgreements();
        };
        $scope.exportData = function() {
            $http({
                method: 'POST',
                url: '/api/agreements.export',
                data: $scope.filterForm,
                responseType: 'arraybuffer'
            }).then(function success(data) {
                let headers = data.headers;
                let link = document.createElement('a');
                try {
                    let blob = new Blob([data.data], {type: headers('content-type') + ';charset=UTF-8'});
                    link.setAttribute('href', $window.URL.createObjectURL(blob));
                    link.setAttribute('download', headers('x-filename'));
                    let clickEvt = new MouseEvent('click', {view: $window, bubbles: true, canceable: false});
                    link.dispatchEvent(clickEvt);
                } catch (e) {
                    console.log(e);
                    toastr.error('Помилка при завантаженні XLS-файлу', 'Помилка запиту');
                }
            }, function error(data) {
                toastr.error('Помилка при створенні XLS-файлу', 'Помилка запиту');
                console.log(data);
            });
        };
        $scope.setPageCb = function(e, page) {
            $scope.filterForm.page = page;
            $scope.loadAgreements();
        };

        $scope.loadAgreements();
    }
])
.controller('AgreementCtrl', ['$scope', '$http', '$routeParams', '$location', 'Page', 'toastr', 'ErrorResponse',
    function AgreementCtrl($scope, $http, $routeParams, $location, Page, toastr, ErrorResponse) {
        $scope.agreementId = $routeParams.agreementId;
        $scope.isNew = $scope.agreementId === 'new';
        Page.setTitle($scope.isNew ? 'Новий договір' : 'Договір №' + $routeParams.agreementId);

        $scope.form = {state: "1", type: "1", executor: {}, sums: {}, prozorro: "0"};
        $scope.select = {codes: {}, accounts: []};
        $scope.pmtForm = {agreementId: $scope.agreementId};
        $scope.pmtEditForm = {};
        $scope.processing = false;

        $scope.paymentAmounts = {};
        $scope.paymentCodes = {};
        $scope.paymentSums = {total: 0};
        $scope.payments = [];

        $scope.notEmptyObject = function(obj) {
            return Object.keys(obj).length > 0;
        };

        $scope._postProcessPayments = function(payments) {
            $scope.payments = payments;
            if ($scope.notEmptyObject(payments)) {
                $scope.paymentCodes = {};
                $scope.paymentSums = {total: 0};
                _.forEach(payments, function(pmt) {
                    $scope.paymentSums.total += pmt.amount;
                    _.forEach(pmt.codes, function(arr, ctype) {
                        $scope.paymentCodes[ctype] = $scope.paymentCodes[ctype] || new Set;
                        _.forEach(arr, function(code) {
                            $scope.paymentCodes[ctype].add(code.code);
                            _.set($scope.paymentAmounts, [pmt.id, code.code], code.amount);
                            $scope.paymentSums[code.code] = _.get($scope.paymentSums, code.code, 0) + code.amount;
                        });
                    });
                });
                $scope.paymentCodes = _.mapValues($scope.paymentCodes, v => [...v]);
            }
        };

        $scope.selectKekv = function(code, action) {
            $scope.selectCode(code, action, 'kekv');
        };

        $scope.selectKdk = function(code, action) {
            $scope.selectCode(code, action, 'kdk');
        };

        $scope.selectKpkv = function(code, action) {
            $scope.selectCode(code, action, 'kpkv');
        };

        $scope.selectCode = function(code, action, type) {
            $scope.$apply(function() {
                switch (action) {
                    case 0: // remove
                        delete $scope.form.sums[type][code.id];
                        if (!$scope.notEmptyObject($scope.form.sums[type])) {
                            delete $scope.form.sums[type];
                        }
                        break;
    
                    case 1: // add
                        if (!$scope.form.sums[type]) {
                            $scope.form.sums[type] = {};
                        }
                        $scope.form.sums[type][code.id] = {code: code.text.split(' ', 1)[0]};
                        break;
                }
            });
        };

        $scope.saveAgreement = function() {
            $scope.processing = true;
            $http.post('/api/agreement.save', $scope.form).then(function success(r) {
                $scope.processing = false;
                if (r.data.status) {
                    toastr.success('Договір збережно успішно!');
                    if ($scope.isNew) {
                        $location.path('/agreements');
                    }
                } else {
                    toastr.error(r.data.message);
                }
            }, function error(r) {
                ErrorResponse.handle(r, $scope);
            });
        };

        $scope.loadAgreement = function() {
            $http.get('/api/agreement.get', {params: {id: $scope.agreementId}}).then(function success(r) {
                if (r.data.status) {
                    Page.setTitle('Договір №' + r.data.data.number);
                    _.forEach(r.data.data, function(value, key) {
                        switch (key) {
                            case 'accounts':
                                $scope.form.accounts = [];
                                $scope.select.accounts = value;
                                for (let i=0; i<value.length; i++) {
                                    $scope.form.accounts.push(value[i].account_id.toString());
                                }
                                break;

                            case 'codes':
                                $scope.form.codes = {};
                                $scope.select.codes = value;
                                _.forEach(value, function(v, k) {
                                    for (let i=0; i<v.length; i++) {
                                        ($scope.form.codes[k] = $scope.form.codes[k] || []).push(v[i].code_id.toString());
                                        ($scope.form.sums[k] = $scope.form.sums[k] || {})[v[i].code_id] = {
                                            code: v[i].code, amount: v[i].amount
                                        };
                                    }
                                });
                                break;

                            case 'payments':
                                $scope._postProcessPayments(value);
                                break;

                            default:
                                $scope.form[key] = Number.isInteger(value) ? value.toString() : value;
                        }
                    });
                }
            }, function error(r) {
                ErrorResponse.handle(r, $scope);
            });
        };

        $scope.addPayment = function() {
            $scope.processing = true;
            $http.post('/api/payment.add', $scope.pmtForm).then(function success(r) {
                $scope.processing = false;
                if (r.data.status) {
                    $scope.pmtForm = {agreementId: $scope.agreementId};
                    toastr.success('Платіж додано успішно!');
                    $scope.reloadPayments();
                } else {
                    toastr.error(r.data.message);
                }
            }, function error(r) {
                ErrorResponse.handle(r, $scope);
            });
        };

        $scope.updatePayment = function() {
            $scope.processing = true;
            $http.post('/api/payment.update', $scope.pmtEditForm).then(function success(r) {
                $scope.processing = false;
                if (r.data.status) {
                    toastr.success('Платіж оновлено успішно.');
                    jQuery('#paymentEditForm').modal('toggle');
                    $scope.reloadPayments();
                } else {
                    toastr.error(r.data.message);
                }
            }, function error(r) {
                ErrorResponse.handle(r, $scope);
            });
        };

        $scope.removePayment = function() {
            $scope.processing = true;
            $http.post('/api/payment.remove', {id: $scope.pmtEditForm.id}).then(function success(r) {
                $scope.processing = false;
                if (r.data.status) {
                    toastr.success('Платіж було видалено!');
                    jQuery('#paymentEditForm').modal('toggle');
                    $scope.reloadPayments();
                } else {
                    toastr.error(r.data.message);
                }
            }, function error(r) {
                ErrorResponse.handle(r, $scope);
            });
        };

        $scope.reloadPayments = function() {
            $http.get('/api/payments.get', {params: {agreementId: $scope.agreementId}}).then(function success(r) {
                if (r.data.status) {
                    $scope._postProcessPayments(r.data.data.payments);
                } else {
                    toastr.error(r.data.message);
                }
            }, function error(r) {
                ErrorResponse.handle(r, $scope);
            });
        };

        $scope.setExecutor = function(item) {
            if (item === null) {
                delete $scope.form.executor.id;
                return;
            }

            $scope.form.executor.id = item.id;
            $scope.$apply(function() {
                $scope.form.executor.taxnum = parseInt(item.taxnum);
            });
        };

        $scope.executorOnBlurExternal = function() {
            if ($scope.form.executor.id) {
                delete $scope.form.executor.id;
                $scope.$apply(function() {
                    $scope.form.executor.taxnum = '';
                });
            }
        };

        $scope.setDate = function(date) {
            jQuery('#iDueDate').datepicker('setStartDate', date);
        };

        $scope.showEditPaymentForm = function(paymentId) {
            let payment = $scope.payments[paymentId];
            Object.assign($scope.pmtEditForm, payment);
            $scope.pmtEditForm.id = paymentId;
            $scope.pmtEditForm.sums = {};
            _.forEach(payment.codes, function(list, type) {
                $scope.pmtEditForm.sums[type] = {};
                _.forEach(list, function(value) {
                    $scope.pmtEditForm.sums[type][value.code_id] = value.amount;
                });
            });
            $scope.pmtEditForm.sums.account = {};
            _.forEach(payment.accounts, function(value) {
                $scope.pmtEditForm.sums.account[value.account_id] = value.amount;
            });
            delete $scope.pmtEditForm.codes;
            delete $scope.pmtEditForm.accounts;
            jQuery('#paymentEditForm').modal('toggle');
        };

        $scope.generateNumber = function() {
            if (!$scope.form.date) {
                toastr.error('Оберіть спочатку дату договору.', 'Помилка');
                return;
            }

            $http.post('/api/agreement.generateNumber', {date: $scope.form.date}).then(function success(r) {
                $scope.form.number = r.data.data.number;
                $scope.genBtnDisabled = true;
            }, function error(r) {
                ErrorResponse.handle(r, $scope);
            });
        };

        if (!$scope.isNew) {
            $scope.loadAgreement();
        }
    }
])
.controller('DKReportsCtrl', ['$scope', '$http', 'Page', 'toastr', 'ErrorResponse',
    function DKReportsCtrl($scope, $http, Page, toastr, ErrorResponse) {
        Page.setTitle('Звіти за КДК');
        $scope.commonFilter = {};
        $scope.planFilter = {};
        $scope.planForm = {};
        $scope.planEditMode = false;
        $scope.processing = false;
        $scope.planData = {};
        $scope.agrmtData = {};
        $scope.searched = false;
        $scope.$planFormPopup = jQuery('#planEditForm');

        let _validate = function() {
            let fieldDesc = {year: 'рік', code: 'КДК'};
            for (let field in fieldDesc) {
                if (!$scope.commonFilter[field]) {
                    toastr.warning('Оберіть ' + fieldDesc[field] + ' для відображення планів та договорів');
                    return false;
                }
            }

            return true;
        };

        $scope.setFilterCode = function(item) {
            if (item === null) {
                delete $scope.commonFilter.code;
                return;
            }

            $scope.commonFilter.code = item.id;
        };
        $scope.openCreatePlanPopup = function() {
            $scope.planForm = {};
            $scope.planEditMode = false;
            $scope.$planFormPopup.modal('toggle');
        };
        $scope.openEditPlanPopup = function(planId) {
            let planObj = _.find($scope.planData.data.plans, {id: planId});
            Object.assign($scope.planForm, planObj);
            $scope.planForm.code = planObj.code.id;
            $scope.planForm.codeText = planObj.code.code + ' ' + planObj.code.description;
            $scope.planEditMode = true;
            $scope.$planFormPopup.modal('toggle');
        };
        $scope.setPlanCode = function(item) {
            if (item === null) {
                delete $scope.planForm.code;
                return;
            }

            $scope.planForm.code = item.id;
        };

        $scope.planSearch = function(skipValidate) {
            if (!skipValidate && !_validate()) {
                return;
            }

            let filter = Object.assign({}, $scope.commonFilter, $scope.planFilter);
            if (Object.keys(filter).length > 0) {
                $http.post('/api/plans', filter).then(function success(r) {
                    Object.assign($scope.planData, r.data);
                }, function error(r) {
                    ErrorResponse.handle(r, $scope);
                });
            } else {
                $http.get('/api/plans').then(function success(r) {
                    Object.assign($scope.planData, r.data);
                }, function error(r) {
                    ErrorResponse.handle(r, $scope);
                });
            }
        };
        $scope.agreementSearch = function() {
            $scope.searched = true;
            $http.post('/api/plan.agreements', $scope.commonFilter).then(function success(r) {
                Object.assign($scope.agrmtData, r.data);
            }, function error(r) {
                ErrorResponse.handle(r, $scope);
            });
        };

        $scope.globalSearch = function() {
            if (!_validate()) {
                return;
            }

            $scope.planSearch(true);
            $scope.agreementSearch();
        };
        $scope.planSave = function() {
            $scope.processing = true;
            $http.post('/api/plan.save', $scope.planForm).then(function success(r) {
                $scope.processing = false;
                if (r.data.status) {
                    $scope.$planFormPopup.modal('toggle');
                    toastr.success('Річний план збережено!');
                    $scope.planSearch(true);
                } else {
                    toastr.error(r.data.message);
                }
            }, function error(r) {
                ErrorResponse.handle(r, $scope);
            });
        };
        $scope.planRemove = function() {
            $scope.processing = true;
            $http.post('/api/plan.remove', {id: $scope.planForm.id}).then(function success(r) {
                $scope.processing = false;
                if (r.data.status) {
                    toastr.success('План було видалено!');
                    $scope.$planFormPopup.modal('toggle');
                    $scope.planSearch(true);
                } else {
                    toastr.error(r.data.message);
                }
            }, function error(r) {
                ErrorResponse.handle(r, $scope);
            });
        };

        $scope.planSearch(true);
    }
]);
