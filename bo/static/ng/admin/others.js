app
.filter('money', ['$filter', function($filter) {
    return function(input) {
        return $filter('currency')(input, '', 2).replace(/,/g, ' ').replace(/\./g, ',');
    };
}])
.service('Page', ['$window', function($window) {
    return {
        setTitle: function(title) {
            $window.document.title = title + ' | Бюджетна Організація';
        }
    };
}])
.service('ErrorResponse', ['toastr', function(toastr) {
    return {
        handle: function(response, $scope) {
            $scope.processing = false;
            if (response.status && response.statusText) {
                if (response.status === 403) {
                    toastr.error('Нажаль, Ви не маєте доступу до цієї функції', 'Помилка запиту');
                    return;
                }

                toastr.error(response.status + ' (' + response.statusText + ')', 'Помилка запиту');
                return;
            }

            toastr.error('Проблема с мережевим підключенням', 'Помилка запиту');
        }
    };
}])
.directive('typeAhead', ['$timeout', '$http', function($timeout, $http) {
    return {
        restrict: 'A',
        scope: { changeCb: '=', onBlurExternal: '=' },
        require: '?ngModel',
        link: function(scope, element, attrs, ngModel) {
            return $timeout(function() {
                $(element).typeahead({
                    items: 'all',
                    minLength: 3,
                    afterSelect: function(item) {
                        scope.changeCb(item, attrs);
                        if (ngModel) {
                            ngModel.$setViewValue(item.name);
                        }
                    },
                    source: attrs.sourceExternal === 'true' ? [] : function(query, callback) {
                        let params = attrs.sourceParams ? angular.fromJson(attrs.sourceParams) : {};
                        params.q = query;
                        $http.post(attrs.sourceUrl, params).then(function(r) {
                            scope.names = [];
                            if (r.data.result) {
                                for (let i=r.data.data.length-1; i>=0; i--) {
                                    scope.names.push(r.data.data[i].name);
                                }
                                return callback(r.data.data);
                            }
                        });
                    }
                }).blur(function() {
                    if (!scope.names) { // external source
                        let source = $(element).data('typeahead').source;
                        if (angular.isArray(source)) {
                            scope.names = [];
                            for (let i=source.length-1; i>=0; i--) {
                                scope.names.push(source[i].name);
                            }
                        }
                    }

                    if (scope.names) {
                        let $el = $(element);
                        if (scope.names.indexOf($el.val()) === -1) {
                            if (!attrs.allowExternal) {
                                $el.val('');
                                scope.changeCb(null, attrs);
                            } else {
                                scope.$apply(function() {
                                    if (ngModel) ngModel.$setViewValue($el.val());
                                });

                                if (scope.onBlurExternal) {
                                    scope.onBlurExternal();
                                }
                            }
                        }
                    }
                });
            });
        }
    };
}])
.directive('datePicker', ['$timeout', function($timeout) {
    return {
        restrict: 'A',
        scope: { changeCb: '=' },
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            return $timeout(function() {
                let $el = $(element);
                $el.datepicker({
                    maxViewMode: 2,
                    language: 'uk',
                    autoclose: true,
                    todayBtn: 'linked',
                    todayHighlight: true,
                    orientation: 'bottom auto'
                }).on('changeDate', function(e) {
                    if (scope.changeCb) {
                        scope.changeCb(e.format());
                    }

                    ngModel.$setViewValue(e.format());
                });

                scope.$watch(function() {
                    return ngModel.$modelValue;
                }, function (value) {
                    if (value) {
                        $el.datepicker('update');
                    }
                });

                let $parent = $el.parent();
                $parent.children('div.input-group-append').click(e => $el.datepicker('show'));
                $parent.children('div.input-group-prepend').click(e => $el.datepicker('show'));
            });
        }
    };
}])
.directive('select2', ['$timeout', function($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: { changeCb: '=' },
        link: function(scope, element, attrs, ngModel) {
            return $timeout(function() {
                $(element).select2({
                    theme: 'bootstrap',
                    width: '100%',
                    minimumInputLength: 2,
                    language: 'uk',
                    multiple: true,
                    placeholder: {
                        text: attrs.placeholder,
                        id: '0'
                    },
                    allowClear: true,
                    ajax: {
                        type: 'post',
                        dataType: 'json',
                        url: attrs.sourceUrl,
                        delay: 250,
                        data: function(params) {
                            let result = attrs.sourceParams ? angular.fromJson(attrs.sourceParams) : {};
                            result.q = params.term;
                            return angular.toJson(result, false);
                        },
                        contentType: 'application/json; charset=utf-8',
                        processResults: function(data) {
                            return {
                                results: data.data
                            }
                        }
                    }
                }).on('change.select2', function() {
                    let val = $(this).val();
                    scope.$apply(function() {
                        ngModel.$setViewValue(val);
                    });
                }).on('select2:select', function(e) {
                    if (scope.changeCb) {
                        scope.changeCb(e.params.data, 1);
                    }
                }).on('select2:unselect', function(e) {
                    if (scope.changeCb) {
                        scope.changeCb(e.params.data, 0);
                    }
                });
            });
        }
    };
}])
.directive('taxnumCheck', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$validators.taxnum = function(modelValue, viewValue) {
                if (!viewValue || (viewValue.length !== 8 && viewValue.length !== 10)) {
                    return true;
                }

                let checkSum = function(val, multiply) {
                    let sum = 0;
                    for (let i=0; i<val.length-1; i++) {
                        sum += val[i] * multiply[i];
                    }
                    return sum % 11;
                };

                let chkSum;
                switch (viewValue.length) {
                    case 8:
                        let multipliers = [
                            [1, 2, 3, 4, 5, 6, 7],
                            [7, 1, 2, 3, 4, 5, 6],
                            [3, 4, 5, 6, 7, 8, 9],
                            [9, 3, 4, 5, 6, 7, 8]
                        ];
                        let mpIdx = (modelValue < 30000000 || modelValue > 60000000) ? 0 : 1;
                        chkSum = checkSum(viewValue, multipliers[mpIdx]);
                        if (chkSum > 9) {
                            chkSum = checkSum(viewValue, multipliers[mpIdx+2]);
                        }

                        break;

                    case 10:
                        chkSum = checkSum(viewValue, [-1, 5, 7, 9, 4, 6, 10, 5, 7]) % 10;
                        break;
                }

                return chkSum === parseInt(viewValue[viewValue.length - 1]);
            };
        }
    };
})
.directive('strToNum', function() {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function(scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function(value) {
                return '' + value;
            });
            ngModel.$formatters.push(function(value) {
                return parseFloat(value);
            });
        }
    };
})
.directive('ignoreMouseWheel', function() {
    return {
        restrict: 'A',
        link: function(scope, element) {
            element.bind('mousewheel', () => element[0].blur());
        }
    };
})
.directive('singleConfirm', ['$timeout', function($timeout) {
    return {
        restrict: 'A',
        scope: { onConfirm: '=' },
        link: function (scope, element) {
            return $timeout(function() {
                $(element).confirmation({
                    rootSelector: '[data-toggle=confirmation]',
                    singleton: true,
                    popout: true,
                    onConfirm: scope.onConfirm,
                    btnCancelIconClass: 'fas fa-times',
                    btnOkIconClass: 'fas fa-check',
                    btnCancelLabel: '&nbsp;НІ',
                    btnOkLabel: '&nbsp;ТАК',
                    title: 'Підтвердження'
                });
            });
        }
    };
}])
.directive('sorted', ['$timeout', function($timeout) {
    return {
        restrict: 'A',
        scope: { setSortingCb: '=' },
        link: function (scope, element, attrs) {
            return $timeout(function() {
                let arrowCls = {desc: 'down', asc: 'up'};
                scope.curDir = attrs.defSort;
                scope.$arrow = $('<i/>');
                scope.$arrow.addClass('clickable fas fa-sort-amount-' + arrowCls[attrs.defSort]);
                scope.$arrow.click(function() {
                    scope.$arrow.removeClass('fa-sort-amount-' + arrowCls[scope.curDir]);
                    scope.curDir = scope.curDir === 'desc' ? 'asc' : 'desc';
                    scope.$arrow.addClass('fa-sort-amount-' + arrowCls[scope.curDir]);
                    if (scope.setSortingCb) {
                        scope.setSortingCb(attrs.sortField, scope.curDir);
                    }
                });
                element.append('&nbsp;');
                element.append(scope.$arrow);
            });
        }
    };
}])
.directive('rowClick', ['$timeout', '$browser', '$window', '$location', function($timeout, $browser, $window, $location) {
    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attrs) {
            return $timeout(function() {
                let onClick = function(e) {
                    if (e.ctrlKey) {
                        $window.open($browser.baseHref() + attrs.href, '_blank');
                    } else {
                        scope.$apply(() => $location.path('/' + attrs.href));
                    }
                };
                element.on('click', onClick);
            });
        }
    };
}])
.directive('paginator', ['$timeout', '$window', function($timeout, $window) {
    return {
        restrict: 'A',
        scope: { setPageCb: '=' },
        link: function(scope, element, attrs) {
            return $timeout(function() {
                let $elem = $(element);
                $elem.bootpag({
                    total: parseInt(attrs.total),
                    maxVisible: parseInt(attrs.maxVisible),
                    wrapClass: 'pagination justify-content-' + attrs.align || 'center',
                    page: 1
                }).on('page', function(e, page) {
                    scope.setPageCb(e, page);
                    if (attrs.topScroll) {
                        setTimeout($window.scroll, 500, {top: 0, behavior: 'smooth'});
                    }
                });

                attrs.$observe('total', function(val) {
                    $elem.bootpag({total: val});
                });

                scope.$on('resetPage', function() {
                    $elem.bootpag({page: 1});
                });
            });
        }
    }
}]);