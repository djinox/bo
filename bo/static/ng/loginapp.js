angular.module('bo', [])
.config(function($interpolateProvider, $compileProvider) {
    if (/bo-odeku\.ga/i.test(window.location.hostname)) {
        $compileProvider.debugInfoEnabled(false);
    }
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
})
.factory('AuthService', ['$q', '$http', function AuthService($q, $http) {
    return ({
        login: function(login, password) {
            let d = $q.defer();
            $http.post('/api/auth', {login: login, password: password})
                .then(function(response) {
                    if (response.status === 200 && response.data.result) {
                        d.resolve(response);
                    }
                }, function(response) {
                    d.reject(response);
                });

            return d.promise;
        }
    });
}])
.controller('LoginCtrl', ['$scope', '$window', 'AuthService',
    function LoginCtrl($scope, $window, AuthService) {
        $scope.loginForm = {};
        $scope.login = function() {
            $scope.error = false;
            $scope.disabled = true;
            AuthService.login($scope.loginForm.login, $scope.loginForm.password)
                .then(function(r) {
                    $window.location.href = r.data.redirect_path;
                }, function(r) { // some shit happened
                    $scope.error = true;
                    $scope.disabled = false;
                    $scope.loginForm = {};
                    $scope.errorMsg = r.data.message || 'Мережева помилка';
                });
        };
    }
]);