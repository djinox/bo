from werkzeug.contrib.fixers import ProxyFix
from flask import g
import redis
import json
import os
import logging


class BaseConf:
    DEBUG = False
    REDIS_SOCKET = '/var/run/redis/redis-server.sock'

    def redis_init(self):
        if 'redis' not in g:
            g.redis = redis.Redis(unix_socket_path=self.REDIS_SOCKET, db=1)

    def post_init(self, app):
        app.before_request(self.redis_init)


class DevelopmentConf(BaseConf):
    DEBUG = True
    SECRET_KEY = 'dev123'
    DATABASE = {
        'host': 'localhost',
        'user': 'bo',
        'password': 'MuzOn.10530',
        'name': 'bo'
    }
    
    def post_init(self, app):
        super().post_init(app)
        logger = logging.getLogger('peewee')
        logger.setLevel(logging.DEBUG)
        logger.addHandler(logging.StreamHandler())


class ProductionConf(BaseConf):
    SECRET_KEY = os.getenv('SECRET_KEY', '')
    DATABASE = json.loads(os.getenv('DATABASE', '{}'))
    SESSION_COOKIE_SECURE = True
    SESSION_COOKIE_HTTPONLY = True
    SESSION_COOKIE_SAMESITE = 'Lax'
    PERMANENT_SESSION_LIFETIME = 86400
    REDIS_SOCKET = os.getenv('REDIS_SOCKET')

    def post_init(self, app):
        super().post_init(app)
        app.wsgi_app = ProxyFix(app.wsgi_app)
