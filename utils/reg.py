import pymysql
import sys
import os
from werkzeug.security import generate_password_hash

conn = pymysql.connect(
    unix_socket='/var/run/mysqld/mysqld.sock',
    user=os.getenv('DBU'),
    password=os.getenv('DBP'),
    db=os.getenv('DBN'),
    charset='utf8mb4'
)
with conn.cursor() as cursor:
    args = sys.argv[1], generate_password_hash(sys.argv[2]), sys.argv[3]
    cursor.execute('insert into user set login=%s,password=%s,state=1,name=%s,permissions=15', args)
    conn.commit()
conn.close()
